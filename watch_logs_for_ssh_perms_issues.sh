#!/usr/bin/env bash
set -euo pipefail

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

message="bad ownership or modes"

/usr/bin/journalctl --unit "sshd" \
                    --output "json" \
                    --output-fields "MESSAGE" \
                    --grep "$message" \
                    --follow \
                    --lines=1 | \
while IFS= read -r line; do
  echo "Found message \"$message\" in the following sshd journalctl output:"
  echo ">>$line<<"
  echo "Calling fix-ssh-permissions.sh command to fix permissions."
  fix-ssh-permissions.sh
done
