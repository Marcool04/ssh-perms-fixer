# SSH permissions fixer

## Rationale

Have you ever been hit with this error when trying to log into a server over SSH:

```
Authentication refused: bad ownership or modes for directory <directory>
```

This little script, timer, and service will make sure that never happens again,
however careless you may be with blundering around your server's file permissions.
It is built from a careful reading of the SSH man page, and should keep all the
file permissions in sync with what the sshd wants to allow safe logins.

## Installation

This script obviously assumes you have OpenSSH's sshd installed (otherwise it's
pointless), as well as systemd and journald.

Installation just involves putting the three files somewhere logical for you,
for example:

```
cp fix-ssh-permissions.sh /usr/bin/
chmod +x /usr/bin/fix-ssh-permissions.sh
cp fix-ssh-permissions.timer /usr/lib/systemd/system/
cp watch_logs_for_ssh_perms_issues.service /usr/lib/systemd/system/
```

## Usage

See `fix-ssh-permissions.sh -h` for some pointers, otherwise for a one-off fix
of the SSH permissions run:

```
fix-ssh-permissions.sh
```

To enable an hourly fix run this:

```
systemctl enable fix-ssh-permissions.timer
```

Or to enable a journalctl log watcher that'll fix everything when any
permission errors are logged run:

```
systemctl enable watch_logs_for_ssh_perms_issues.service
```

## Improvements? Other files?

Has your ssh server stopped you from logging in because of another file
that this utility doesn't fix? Please let me know in the issues and I'd
be glad to add it. Just create a pull request or an issue on this repo.
