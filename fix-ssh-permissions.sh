#!/usr/bin/env bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

set -eu

if ! which fd >/dev/null; then
  echo "Utility \"fd \" not found. Please install it and try again."
  exit 10
fi

function do_fix {
  ssh_folder="$1"
  if ! [[ -e "$ssh_folder" ]]; then
    echo "Folder $ssh_folder doesn't exist. Skipping."
    return
  fi
  echo "Working on ssh folder $ssh_folder"
  home_folder="$(dirname "$ssh_folder")"

  user="$(stat -c "%U" "$home_folder")"

  # Comments are from the man page sshd(8)

  # If [the ~/.ssh/authorized_keys] file, the ~/.ssh directory,
  # or the user's home directory are writable by other users,
  # then the file could be modified or replaced by unauthorized users.
  # In this case, sshd will not allow it to be used unless the
  # StrictModes option has been set to “no”.
  echo "This folder will be chown-ed to $user and chmod-ed to 750: $home_folder"
  chown "${user}:${user}" "$home_folder"
  chmod 750 "$home_folder"

  # ~/.rhosts
  # This file is used for host-based authentication (see ssh(1) for more information).
  # On some machines this file may need to be world-readable if the user's home directory
  # is on an NFS partition, because sshd reads it as root.  Additionally, this file must
  # be owned by the user, and must not have write permissions for anyone else.
  # The recommended permission for most machines is read/write for the user, and not
  # accessible by others.
  fd --no-ignore --hidden --type f --max-depth=1 "^.rhost$" "$home_folder" | while read -r rhosts; do
    echo "This file will be chowned to user $user, and chmod-ed to 700: $rhosts"
    echo "WARNING: this is wrong if you are using NFS on this folder!"
    chown "$user" "$rhosts"
    chmod 700 "$rhosts"
  done

  # ~/.ssh/
  #  There is no general requirement to keep the entire contents of this directory
  # secret, but the recommended permissions are read/write/execute for the user,
  # and not accessible by others.
  echo "This folder will be chown-ed to $user and chmod-ed to 700: $ssh_folder"
  chown -R "${user}:${user}" "$ssh_folder"
  chmod 700 "$ssh_folder"

  # ~/.ssh/authorized_keys
  #  […] The content of the file is not highly sensitive, but the recommended
  # permissions are read/write for the user, and not accessible by others.
  fd --no-ignore --hidden --type f --max-depth=1 "^authorized_keys$" "$ssh_folder" | while read -r authorized_keys_file; do
    echo "This file will be chown-ed to $user and chmod-ed to 600: $authorized_keys_file"
    chown "${user}:${user}" "$authorized_keys_file"
    chmod 600 "$authorized_keys_file"
  done
  echo "Done for $ssh_folder"
  echo "####################"
}

if [[ -n "$1" ]]; then
  ssh_folder="$1"
  echo "Working on folder $ssh_folder"
  if ! [[ -d "$1" ]]; then
    echo "Folder doesn't exist, failing."
    exit 10
  fi
  do_fix "$ssh_folder"
  exit 0
fi

echo "Searching for folders..."
fd --no-ignore --hidden --type d --max-depth=2 "^\.ssh$" "/home/" | while read -r ssh_folder; do
  do_fix "$ssh_folder"
done

do_fix "/root/.ssh"

echo "All done."

exit 0
